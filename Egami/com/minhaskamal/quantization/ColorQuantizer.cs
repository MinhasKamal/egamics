/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
* Date: Dec-2015                                           *
***********************************************************/

using System.Collections;
using Egami.com.minhaskamal.matrix;

namespace Egami.com.minhaskamal.quantization {
    public class ColorQuantizer {

        /**
         * 
         * @param matrix only grey-scale
         * @param classRangeAndValues <code> List < int[] {lower-bound, upper-bound, symbol-value} > </code>
         * @param outOfRangeValue
         * @return
         */
        public static Matrix symbolize(Matrix matrix, ArrayList classRangeAndValues, int outOfRangeValue) {
            matrix=MatrixTypeConverter.convert(matrix, Matrix.BLACK_WHITE);

            Matrix matrix2 = new Matrix(matrix.Row, matrix.Col, Matrix.BLACK_WHITE);

            for(int i = 0, j; i<matrix.Row; i++) {
                for(j=0; j<matrix.Col; j++) {

                    int newPixel = -999;

                    int oldPixel = matrix.pixels[i, j, 0];
                    foreach(int[] classRangeAndValue in classRangeAndValues) {
                        if(oldPixel>=classRangeAndValue[0] && oldPixel<=classRangeAndValue[1]) {
                            newPixel=classRangeAndValue[2];
                            break;
                        }
                    }

                    if(newPixel<-990) {
                        newPixel=outOfRangeValue;
                    }

                    matrix2.pixels[i, j, 0]=newPixel;
                }
            }

            return matrix2;
        }

        public static Matrix convertToBinary(Matrix matrix, int threshold) {
            ArrayList classRangeAndValues = new ArrayList();
            classRangeAndValues.Add(new int[] { 0, threshold, Matrix.MIN_PIXEL });
            classRangeAndValues.Add(new int[] { threshold, 256, Matrix.MAX_PIXEL });

            return symbolize(matrix, classRangeAndValues, 150);
        }
    }
}
