/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
* Date: Dec-2015                                           *
* Modification Date: 04-Oct-2016                           *
* License: MIT License                                     *
***********************************************************/

namespace Egami.com.minhaskamal.matrix {
	public class MatrixUtilities {

		////----pixel value operation----////


		/**
		 * Returns number of occurrences of all the pixels in a Matrix.
		 * @param matrix histogram of matrix
		 * @return
		 */
		public static int[][] countPixelFreq(Matrix matrix) {

			int[][] pixelFreq = new int[matrix.Type][];
			for(int k = 0; k<matrix.Type; k++) {
				pixelFreq[k]=new int[Matrix.MAX_PIXEL+1];
				for(int i = 0; i<matrix.Row; i++) {
					for(int j = 0; j<matrix.Col; j++) {
						pixelFreq[k][matrix.pixels[i, j, k]]++;
					}
				}
			}

			return pixelFreq;
		}

		/**
		 * Returns the average of all pixels of a Matrix.
		 * @param mat
		 * @return
		 */
		public static double getMeanPixelValue(Matrix matrix) {

			double sumOfPixelByRow;
			double sumOfPixelByColInRow = 0;
			for(int x = 0, y, z; x<matrix.Row; x++) {
				sumOfPixelByRow=0;
				for(y=0; y<matrix.Col; y++) {
					for(z=0; z<matrix.Type; z++) {
						sumOfPixelByRow+=matrix.pixels[x, y, z];
					}
				}

				sumOfPixelByColInRow+=sumOfPixelByRow/(matrix.Type*matrix.Col);
			}

			return sumOfPixelByColInRow/matrix.Row;
		}

		/**
		 * The minimum and maximum pixel value of input
		 * <code>Matrix</code> is returned as array.
		 * @param matrix
		 * @return
		 */
		public static int[] getMinMaxPixelValue(Matrix matrix) {

			int MinPixel = int.MaxValue,
				MaxPixel = int.MinValue;
			for(int i = 0; i<matrix.Row; i++) {
				for(int j = 0; j<matrix.Col; j++) {
					for(int k = 0; k<matrix.Type; k++) {
						if(MinPixel>matrix.pixels[i, j, k]) {
							MinPixel=matrix.pixels[i, j, k];
						}
						if(MaxPixel<matrix.pixels[i, j, k]) {
							MaxPixel=matrix.pixels[i, j, k];
						}
					}
				}
			}

			return new int[] { MinPixel, MaxPixel };
		}


		////----pixel creation----////


		/**
		 * Returns a pixel of the same type as input Matrix, all carrying the 
		 * same <code>pixelValue</code>.
		 * @param matrix
		 * @param pixelValue
		 * @return
		 */
		public static int[] getCustomPixel(Matrix matrix, int pixelValue) {
			int[] customPixel = new int[matrix.Type];

			for(int i = 0; i<customPixel.Length; i++) {
				customPixel[i]=pixelValue;
			}

			return customPixel;
		}

		public static int[] getBlackPixel(Matrix matrix) {
			return getCustomPixel(matrix, Matrix.MIN_PIXEL);
		}

		public static int[] getWhitePixel(Matrix matrix) {
			return getCustomPixel(matrix, Matrix.MAX_PIXEL);
		}


		////----matrix creation----////

		/**
		 * Creates a Matrix of the same size and type of input Matrix,
		 * and all pixel values are initiated with the <code>initialValue</code>.
		 * @param matrix
		 * @param initialValue
		 * @return
		 */
		public static Matrix createNewMatrix(Matrix matrix, int initialValue) {

			Matrix matrix2 = new Matrix(matrix.Row, matrix.Col, matrix.Type);
			for(int i = 0; i<matrix.Row; i++) {
				for(int j = 0; j<matrix.Col; j++) {
					for(int k = 0; k<matrix.Type; k++) {
						matrix2.pixels[i, j, k]=initialValue;
					}
				}
			}

			return matrix2;
		}

		public static Matrix createEmptyMatrix(Matrix matrix) {
			return new Matrix(matrix.Row, matrix.Col, matrix.Type);
		}


		////----matrix data transformation----////

		/**
		 * Converts Matrix to a vector of pixels.
		 * @param matrix
		 * @return
		 */
		public static int[][] vectorize(Matrix matrix) {

			int[][] vector = new int[matrix.Row*matrix.Col][];

			for(int i = 0, l = 0; i<matrix.Row; i++) {
				for(int j = 0; j<matrix.Col; j++) {
					vector[l]=new int[matrix.Type];
					for(int k = 0; k<matrix.Type; k++) {
						vector[l][k]=matrix.pixels[i, j, k];
					}
					l++;
				}
			}

			return vector;
		}

		/**
		 * Creates Matrix from a vector of pixels.
		 * @param vector
		 * @param row
		 * @param col
		 * @return
		 */
		public static Matrix createMatrix(int[][] vector, int row, int col) {

			Matrix matrix = new Matrix(row, col, vector[0].Length);

			int l = 0;
			for(int i = 0; i<row; i++) {
				for(int j = 0; j<col; j++) {
					matrix.setPixel(i, j, vector[l]);
					l++;
				}
			}

			return matrix;
		}


		////----matrix operation----////

		/**
		 * Scales pixel values of input Matrix in <code>Matrix.MIN_PIXEL</code> to 
		 * <code>Matrix.MAX_PIXEL</code>.
		 * @param matrix
		 * @return
		 */
		public static Matrix normalizeMatrix(Matrix matrix) {

			int[] minMaxValue = getMinMaxPixelValue(matrix);

			double multiple = 0;
			if(minMaxValue[1]-minMaxValue[0]!=0) {
				multiple=(double)(Matrix.MAX_PIXEL-Matrix.MIN_PIXEL)/(minMaxValue[1]-minMaxValue[0]);
			}

			for(int i = 0; i<matrix.Row; i++) {
				for(int j = 0; j<matrix.Col; j++) {
					for(int k = 0; k<matrix.Type; k++) {
						matrix.pixels[i, j, k]-=minMaxValue[0];
						matrix.pixels[i, j, k]=(int)(matrix.pixels[i, j, k]*multiple);
					}
				}
			}

			return matrix;
		}

	}
}