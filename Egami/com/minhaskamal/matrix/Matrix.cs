/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
* Date: 10-Mar-2015                                        *
* Modification Date: 30-Dec-2015                           *
* Modification Date: 22-Jan-2016                           *
* Modification Date: 03-Oct-2016                           *
* Modification Date: 03-Feb-2017                           *
* License: MIT License                                     *
***********************************************************/

using System.Drawing;

namespace Egami.com.minhaskamal.matrix {

    public class Matrix {
        /**
		 * Matrix types- <br/>
		 * 1. <code>BLACK_WHITE</code> -> grey-level image <br/>
		 * 2. <code>BLACK_WHITE_ALPHA</code> -> grey-level image with opacity <br/>
		 * 3. <code>RED_GREEN_BLUE</code> -> color image <br/>
		 * 4. <code>RED_GREEN_BLUE_ALPHA</code> color image with opacity <br/>
		 */
        public static readonly int BLACK_WHITE = 1,
                BLACK_WHITE_ALPHA = 2,
                RED_GREEN_BLUE = 3,
                RED_GREEN_BLUE_ALPHA = 4;

        /**
		 * <code>WHITE_PIXEL = 255</code>, highest value of a <code>pixel</code> <br/>
		 * <code>BLACK_PIXEL = 0</code>, lowest value of a <code>pixel</code>
		 */
        public static readonly int MAX_PIXEL = 255,
                MIN_PIXEL = 0;

        /**
		 * first dimension represents row or height from top to bottom ranging from 0 to height-1,
		 * second dimension represents column or width from left to right ranging from 0 to width-1,
		 * third dimension represents intensity value.
		 */
        public int[,,] pixels;

        public readonly int Row, Col, Type;


        ////----constructors----////


        public Matrix(int[,,] pixels) {
            this.pixels=pixels;
            this.Row=pixels.GetLength(0);
            this.Col=pixels.GetLength(1);
            this.Type=pixels.GetLength(2);
        }

        /**
		 * When invalid info is provided Matrix is created with minimal size.
		 * @param row height of the image
		 * @param col width of the image
		 * @param type <code>BLACK_WHITE</code>, <code>BLACK_WHITE_ALPHA</code>, 
		 * <code>RED_GREEN_BLUE</code>, <code>RED_GREEN_BLUE_ALPHA</code>
		 */
        public Matrix(int row, int col, int type) : this(new int[row, col, type]){
        }

        public Matrix(string filePath, int type) : this(new Bitmap(filePath), type) {
        }

        public Matrix(Bitmap bitmap, int type) : this(bitmapToMatrix(bitmap, type).pixels){
        }

        ////----getter setters----////

        /**
		 * @param rowNo 0 =< rowNo < number of rows
		 * @param colNo 0 =< colNo < number of columns
		 */
        public int[] getPixel(int rowNo, int colNo) {
            int[] pixel = new int[Type];
            for(int i = 0; i<pixel.Length; i++) {
                pixel[i]=this.pixels[rowNo, colNo, i];
            }
            return pixel;
        }

        /**
		 * @param rowNo 0 =< rowNo < number of rows
		 * @param colNo 0 =< colNo < number of columns
		 */
        public void setPixel(int rowNo, int colNo, int[] pixel) {
            for(int i = 0; i<pixel.Length; i++) {
                this.pixels[rowNo, colNo, i]=pixel[i];
            }
        }


        ////----matrix-bufferedimage methods----////


        /**
		 * Creates <code>BufferedImage</code> from Matrix.
		 * @param matrix input matrix
		 * @return
		 */
        public static Bitmap matrixToBitmap(Matrix matrix) {
            Bitmap bitmap = new Bitmap(matrix.Col, matrix.Row);

            if(matrix.Type==Matrix.BLACK_WHITE) {
                for(int i = 0, j; i<matrix.Row; i++) {
                    for(j=0; j<matrix.Col; j++) {
                        bitmap.SetPixel(j, i, Color.FromArgb(Matrix.MAX_PIXEL,
                            matrix.pixels[i, j, 0], matrix.pixels[i, j, 0], matrix.pixels[i, j, 0]));
                    }
                }
            }
            else if(matrix.Type==Matrix.BLACK_WHITE_ALPHA) {
                for(int i = 0, j; i<matrix.Row; i++) {
                    for(j=0; j<matrix.Col; j++) {
                        bitmap.SetPixel(j, i, Color.FromArgb(matrix.pixels[i, j, 1],
                            matrix.pixels[i, j, 0], matrix.pixels[i, j, 0], matrix.pixels[i, j, 0]));
                    }
                }
            }
            else if(matrix.Type==Matrix.RED_GREEN_BLUE) {
                for(int i = 0, j; i<matrix.Row; i++) {
                    for(j=0; j<matrix.Col; j++) {
                        bitmap.SetPixel(j, i, Color.FromArgb(Matrix.MAX_PIXEL,
                            matrix.pixels[i, j, 0], matrix.pixels[i, j, 1], matrix.pixels[i, j, 2]));
                    }
                }
            }
            else {
                for(int i = 0, j; i<matrix.Row; i++) {
                    for(j=0; j<matrix.Col; j++) {
                        bitmap.SetPixel(j, i, Color.FromArgb(matrix.pixels[i, j, 3],
                            matrix.pixels[i, j, 0], matrix.pixels[i, j, 1], matrix.pixels[i, j, 2]));
                    }
                }
            }

            return bitmap;
        }

        /**
		 * Creates Matrix from <code>BufferedImage</code>
		 * @param bufferedImage
		 * @param type type of the output Matrix
		 * @return
		 */
        public static Matrix bitmapToMatrix(Bitmap bitmap, int type) {
            int row = bitmap.Height;
            int col = bitmap.Width;

            if(type<Matrix.BLACK_WHITE||type>Matrix.RED_GREEN_BLUE_ALPHA) {
                type=Matrix.BLACK_WHITE;
            }

            Matrix matrix = new Matrix(row, col, type);
            Color color;

            if(type==BLACK_WHITE) {
                for(int i = 0, j; i<row; i++) {
                    for(j=0; j<col; j++) {
                        color=bitmap.GetPixel(j, i);
                        matrix.pixels[i, j, 0]=color.G;
                    }
                }
            }
            else if(type==BLACK_WHITE_ALPHA) {
                for(int i = 0, j; i<row; i++) {
                    for(j=0; j<col; j++) {
                        color=bitmap.GetPixel(j, i);
                        matrix.pixels[i, j, 0]=color.G;
                        matrix.pixels[i, j, 1]=color.A;
                    }
                }
            }
            else if(type==RED_GREEN_BLUE) {
                for(int i = 0, j; i<row; i++) {
                    for(j=0; j<col; j++) {
                        color=bitmap.GetPixel(j, i);
                        matrix.pixels[i, j, 0]=color.R;
                        matrix.pixels[i, j, 1]=color.G;
                        matrix.pixels[i, j, 2]=color.B;
                    }
                }
            }
            else {
                for(int i = 0, j; i<row; i++) {
                    for(j=0; j<col; j++) {
                        color=bitmap.GetPixel(j, i);
                        matrix.pixels[i, j, 0]=color.R;
                        matrix.pixels[i, j, 1]=color.G;
                        matrix.pixels[i, j, 2]=color.B;
                        matrix.pixels[i, j, 3]=color.A;
                    }
                }
            }

            return matrix;
        }

        ////----methods----////

        /**
		 * Crops a section out of the Matrix.
		 * @param rowStart 0 =< rowStart < rowEnd
		 * @param rowEnd rowStart < rowEnd <= numberOfRows-1
		 * @param colStart 0 =< colStart < colEnd
		 * @param colEnd colStart < colEnd <= numberOfCols-1
		 * @return a new Matrix
		 */
        public Matrix subMatrix(int rowStart, int rowEnd, int colStart, int colEnd) {
            if(rowStart>=0&&colStart>=0&&rowEnd<=this.Row&&colEnd<=this.Col&&
                rowEnd>rowStart&&colEnd>colStart) {

                int row = rowEnd-rowStart;
                int col = colEnd-colStart;
                int type = this.Type;

                Matrix matrix = new Matrix(row, col, type);

                for(int i = 0; i<row; i++) {
                    for(int j = 0; j<col; j++) {
                        for(int k = 0; k<type; k++) {
                            matrix.pixels[i, j, k]=this.pixels[rowStart+i, colStart+j, k];
                        }
                    }
                }

                return matrix;

            }
            else {
                return null;
            }
        }

        /**
		 * Clones the entire Matrix into a new instance.
		 */
        public Matrix clone() {
            return subMatrix(0, this.Row, 0, this.Col);
        }

        ////----io operation----////

        /**
		 * Writes image in the hard-disk.
		 * @param filePath
		 * @throws IOException
		 */
        public void write(string filePath) {
            matrixToBitmap(this).Save(filePath);
        }

        /**
		 * @return Matrix as text
		 */
        public string dump() {

            string str = "("+this.Row+","+this.Col+","+this.Type+")";
            str+="[";

            string pixelString, rowPixelsString;
            for(int i = 0, j; i<this.Row; i++) {

                rowPixelsString="";
                for(j=0; j<this.Col; j++) {

                    pixelString="";
                    for(int k = 0; k<this.Type; k++) {
                        pixelString+=this.pixels[i, j, k]+",";
                    }

                    rowPixelsString+=pixelString;
                }

                str+=rowPixelsString;
            }

            str+="]";

            return str;
        }

        /**
		 * Creates Matrix from text.
		 * @param string output of <code>dump()</code>
		 */
        public static Matrix load(string str) {
            int startIndex = 0, stopIndex = 0;
            int row = 1, col = 1, type = 1;

            startIndex=1;
            stopIndex=str.IndexOf(',', startIndex);
            row=int.Parse(str.Substring(startIndex, stopIndex));

            startIndex=stopIndex+1;
            stopIndex=str.IndexOf(',', startIndex);
            col=int.Parse(str.Substring(startIndex, stopIndex));

            startIndex=stopIndex+1;
            stopIndex=str.IndexOf(')', startIndex);
            type=int.Parse(str.Substring(startIndex, stopIndex));



            Matrix matrix = new Matrix(row, col, type);

            stopIndex=str.IndexOf('[', stopIndex);
            for(int i = 0; i<row; i++) {
                for(int j = 0; j<col; j++) {

                    for(int k = 0; k<type; k++) {
                        startIndex=stopIndex+1;
                        stopIndex=str.IndexOf(',', startIndex);

                        matrix.pixels[i, j, k]=int.Parse(str.Substring(startIndex, stopIndex));
                    }
                }
            }

            return matrix;
        }

        /**
		 * Visualizes image as text.
		 */
        public string toString() {
            
            string str = "("+this.Row+","+this.Col+","+this.Type+")\n";
            str+="[\n";

            string pixelString, rowPixelsString;
            for(int i = 0, j, k; i<this.Row; i++) {

                rowPixelsString="[";
                for(j=0; j<this.Col; j++) {

                    pixelString="("+this.pixels[i, j, 0];
                    for(k=1; k<this.Type; k++) {
                        pixelString+="."+this.pixels[i, j, k];
                    }

                    rowPixelsString+=pixelString+") ";
                }

                str+=rowPixelsString+"]\n";
            }

            str+="]";

            return str;
        }


    }
}