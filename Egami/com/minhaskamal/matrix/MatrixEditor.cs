/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
* Date: Dec-2015                                           *
* Modification Date: 05-Oct-2016                           *
* License: MIT License                                     *
***********************************************************/

using System;
using System.Drawing;

namespace Egami.com.minhaskamal.matrix {
    public class MatrixEditor {

        ////----crop----////


        public static Matrix crop(Matrix matrix, int top, int right, int down, int left) {
            return matrix.subMatrix(top, matrix.Row-down, left, matrix.Col-right);
        }


        ////----border----////


        public static Matrix createBorder(Matrix matrix, int breadth, int pixelValue) {
            return createBorder(matrix, breadth, breadth, pixelValue);
        }

        /**
		 * 
		 * @param horizontalBreadth left and right side breadth
		 * @param verticalBreadth top and bottom side breadth
		 * @param borderPixel
		 * @return
		 */
        public static Matrix createBorder(Matrix matrix, int horizontalBreadth, int verticalBreadth, int pixelValue) {
            int[] borderPixel = MatrixUtilities.getCustomPixel(matrix, pixelValue);

            return createBorder(matrix, verticalBreadth, horizontalBreadth, verticalBreadth, horizontalBreadth, borderPixel);
        }

        public static Matrix createBorder(Matrix matrix, int topBreadth, int rightBreadth, int bottomBreadth, int leftBreadth,
                int[] borderPixel) {
            if(borderPixel.Length!=matrix.Type) {
                return matrix;
            }

            int newRows = matrix.Row+topBreadth+bottomBreadth,
                newCols = matrix.Col+rightBreadth+leftBreadth;

            Matrix matrix2 = new Matrix(newRows, newCols, matrix.Type);

            for(int i = 0; i<newRows; i++) {
                for(int j = 0; j<newCols; j++) {
                    matrix2.setPixel(i, j, borderPixel);
                }
            }

            for(int i = 0; i<matrix.Row; i++) {
                for(int j = 0; j<matrix.Col; j++) {
                    for(int k = 0; k<matrix.Type; k++) {
                        matrix2.pixels[i+topBreadth, j+leftBreadth, k]=matrix.pixels[i, j, k];
                    }
                }
            }

            return matrix2;
        }


        ////----rotate----////

        //full rotate//
        /**
		 * Rotate Matrix 90 degree to the left.
		 * @param matrix
		 * @return
		 */
        public static Matrix rotateLeft(Matrix matrix) {

            Matrix matrix2 = new Matrix(matrix.Col, matrix.Row, matrix.Type);
            for(int i = 0; i<matrix.Col; i++) {
                int i2 = matrix.Col-1-i;
                for(int j = 0; j<matrix.Row; j++) {
                    for(int k = 0; k<matrix.Type; k++) {
                        matrix2.pixels[i2, j, k]=matrix.pixels[j, i, k];
                    }
                }
            }

            return matrix2;
        }

        /**
		 * Rotate Matrix 90 degree to the right.
		 * @param matrix
		 * @return
		 */
        public static Matrix rotateRight(Matrix matrix) {

            Matrix matrix2 = new Matrix(matrix.Col, matrix.Row, matrix.Type);
            for(int j = 0; j<matrix.Row; j++) {
                int j2 = matrix.Row-1-j;
                for(int i = 0; i<matrix.Col; i++) {
                    for(int k = 0; k<matrix.Type; k++) {
                        matrix2.pixels[i, j2, k]=matrix.pixels[j, i, k];
                    }
                }
            }

            return matrix2;
        }

        //flip//
        /**
		 * Flip a Matrix top to bottom.
		 * @param matrix
		 * @return
		 */
        public static Matrix flipVertical(Matrix matrix) {
            int row = matrix.Row,
                col = matrix.Col;

            Matrix matrix2 = new Matrix(matrix.Row, matrix.Col, matrix.Type);
            for(int i = 0; i<matrix.Row; i++) {
                int i2 = matrix.Row-1-i;
                for(int j = 0; j<matrix.Col; j++) {
                    for(int k = 0; k<matrix.Type; k++) {
                        matrix2.pixels[i2, j, k]=matrix.pixels[i, j, k];
                    }
                }
            }

            return matrix2;
        }

        /**
		 * Flip a Matrix left to right.
		 * @param matrix
		 * @return
		 */
        public static Matrix flipHorizontal(Matrix matrix) {

            Matrix matrix2 = new Matrix(matrix.Row, matrix.Col, matrix.Type);
            for(int j = 0; j<matrix.Col; j++) {
                int j2 = matrix.Col-1-j;
                for(int i = 0; i<matrix.Row; i++) {
                    for(int k = 0; k<matrix.Type; k++) {
                        matrix2.pixels[i, j2, k]=matrix.pixels[i, j, k];
                    }
                }
            }

            return matrix2;
        }

        //skew//
        public static Matrix skewHorizontal(Matrix matrix, double angle) {
            
            angle%=90;
            double tanX = Math.Tan(toRadians(angle));
            Matrix matrix2 = new Matrix(matrix.Row, (int)(matrix.Col+(matrix.Row*Math.Abs(tanX))), matrix.Type);

            int x = 0;
            if(angle<0) {
                x=(int)(matrix.Row*Math.Abs(tanX));
            }
            for(int i = 0; i<matrix.Row; i++) {
                int skew = (int)(i*tanX)+x;
                for(int j = 0; j<matrix.Col; j++) {
                    for(int k = 0; k<matrix.Type; k++) {
                        matrix2.pixels[i, j+skew, k]=matrix.pixels[i, j, k];
                    }
                }
            }

            return matrix2;
        }


        public static Matrix skewVertical(Matrix matrix, double angle) {
           
            angle%=90;
            double tanX = Math.Tan(toRadians(angle));
            Matrix matrix2 = new Matrix((int)(matrix.Row+(matrix.Col*Math.Abs(tanX))), matrix.Col, matrix.Type);

            int y = 0;
            if(angle<0) {
                y=(int)(matrix.Col*Math.Abs(tanX));
            }
            for(int j = 0; j<matrix.Col; j++) {
                int skew = (int)((matrix.Col-1-j)*tanX)+y;
                for(int i = 0; i<matrix.Row; i++) {
                    for(int k = 0; k<matrix.Type; k++) {
                        matrix2.pixels[i+skew, j, k]=matrix.pixels[i, j, k];
                    }
                }
            }

            return matrix2;
        }

        //rotate//

        public static Matrix rotate(Matrix matrix, double angle) {
            angle%=360;
            if(angle<0) {
                angle+=360;
            }

            Matrix matrix2 = fullRotate(matrix, angle);

            angle%=90;
            if(angle==0) {
                return matrix2;
            }

            Matrix matrix3 = smallRotate(matrix2, angle);
            int cropTop = (int)Math.Floor(matrix2.Row*(1-Math.Cos(toRadians(angle))));

            return matrix3.subMatrix(cropTop, matrix3.Row, 0, matrix3.Col);
        }

        /**
		 * If no rotation happens then returns the same Matrix.
		 */
        private static Matrix fullRotate(Matrix matrix, double angle) {
            Matrix matrix2;
            if(angle>=270) {
                matrix2=rotateLeft(matrix);
            }
            else if(angle>=180) {
                matrix2=rotateRight(rotateRight(matrix));
            }
            else if(angle>=90) {
                matrix2=rotateRight(matrix);
            }
            else {
                matrix2=matrix;
            }

            return matrix2;
        }

        /**
		 * Returns new Matrix.
		 */
        private static Matrix smallRotate(Matrix matrix, double angle) {
            Bitmap originalBitmap = Matrix.matrixToBitmap(matrix);
            Bitmap rotatedBitmap = new Bitmap(originalBitmap.Width, originalBitmap.Height, originalBitmap.PixelFormat);
            /// @TODO
            /*AffineTransform affineTransform = new AffineTransform();
			affineTransform.rotate(toRadians(angle), 0, rotatedBitmap.Height);
			AffineTransformOp affineTransformOp = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BILINEAR);
			rotatedBitmap = affineTransformOp.filter(originalBitmap, null);*/

            return Matrix.bitmapToMatrix(rotatedBitmap, matrix.Type);
        }


        ////----resize----////


        public static Matrix resize(Matrix matrix, int newWidth, int newHeight) {
            Bitmap originalBitmap = Matrix.matrixToBitmap(matrix);
            Bitmap resizedBitmap = new Bitmap(originalBitmap, newWidth, newHeight);

            return Matrix.bitmapToMatrix(resizedBitmap, matrix.Type);
        }

        public static Matrix resize(Matrix matrix, double percentage) {
            return resize(matrix, percentage, percentage);
        }

        public static Matrix resize(Matrix matrix, double widthPercentage, double heightPercentage) {
            int newWidth = (int)(matrix.Col*widthPercentage),
            newHeight = (int)(matrix.Row*heightPercentage);

            return resize(matrix, newWidth, newHeight);
        }

        public static double toRadians(double degree) {
            return Math.PI*degree/180;
        }
    }
}