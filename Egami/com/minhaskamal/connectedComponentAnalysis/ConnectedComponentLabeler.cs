/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
* Date: 01-Apr-2016                                        *
* License: MIT License                                     *
***********************************************************/

using System.Collections.Generic;
using System.Collections;
using Egami.com.minhaskamal.matrix;

namespace Egami.com.minhaskamal.connectedComponentAnalysis {
    public class ConnectedComponentLabeler {

        private Matrix matrix;
        private ArrayList connectedPixelGroups;

        public ConnectedComponentLabeler(Matrix matrix) {
            this.matrix=MatrixTypeConverter.convert(matrix, Matrix.BLACK_WHITE);
            this.connectedPixelGroups=new ArrayList();
        }


        public void extractPixelGroup(int threshold, int minWidth, int minHeight, int maxWidth, int maxHeight) {
            extractPixelGroup(threshold, minWidth, minHeight, maxWidth, maxHeight,
                    int.MinValue, int.MaxValue, int.MinValue, int.MaxValue);
        }

        public void extractPixelGroup(int threshold,
                int minWidth, int minHeight, int maxWidth, int maxHeight,
                int minNumberOfPixels, int maxNumberOfPixels, int minPixelPercentage, int maxPixelPercentage) {

            int rows = this.matrix.Row,
                cols = this.matrix.Col;


            Matrix borderedMatrix = MatrixEditor.createBorder(this.matrix, 1, Matrix.MAX_PIXEL);

            for(int i = 0; i<this.matrix.Row; i++) {
                for(int j = 0; j<this.matrix.Col; j++) {

                    if(borderedMatrix.pixels[i, j, 0]<threshold) {
                        ArrayList connectedDots = getConnectedPixelGroup(borderedMatrix, threshold, i, j);
                        int[] minXMaxXMinYMaxY = getMinXMaxXMinYMaxY(connectedDots);

                        int width = minXMaxXMinYMaxY[1]-minXMaxXMinYMaxY[0]+1,
                            height = minXMaxXMinYMaxY[3]-minXMaxXMinYMaxY[2]+1,
                            numberOfPixels = connectedDots.Count,
                            pixelPercentage = numberOfPixels*100/(width*height);

                        if(width>minWidth&&width<maxWidth&&height>minHeight&&height<maxHeight&&
                                numberOfPixels>minNumberOfPixels&&numberOfPixels<maxNumberOfPixels&&
                                pixelPercentage>minPixelPercentage&&pixelPercentage<maxPixelPercentage) {

                            this.connectedPixelGroups.Add(connectedDots);
                        }
                    }

                }
            }

            return;
        }

        private ArrayList getConnectedPixelGroup(Matrix matrix, int threshold, int i, int j) {
            ArrayList blackDots = new ArrayList();
            LinkedList<int[]> blackDotStack = new LinkedList<int[]>();

            blackDotStack.AddLast(new int[] { i, j });
            matrix.pixels[i, j, 0]=Matrix.MAX_PIXEL;
            while(blackDotStack.Count>0) {
				int[] blackDot = blackDotStack.First.Value;
				blackDotStack.RemoveFirst();
				blackDots.Add(blackDot);

                for(int k = -1; k<2; k++) {
                    for(int l = -1; l<2; l++) {

                        if(matrix.pixels[blackDot[0]+k, blackDot[1]+l, 0]<threshold) {
                            blackDotStack.AddLast(new int[] { blackDot[0]+k, blackDot[1]+l });
                            matrix.pixels[blackDot[0]+k, blackDot[1]+l, 0]=Matrix.MAX_PIXEL;
                        }
                    }
                }
            }

            return blackDots;
        }

        /**
         * Calculates boundary of connected dots.
         * @param connectedDots
         * @return
         */
        private int[] getMinXMaxXMinYMaxY(ArrayList connectedDots) {
            int minX = ((int[])connectedDots[0])[0],
                maxX = ((int[])connectedDots[0])[0],
                minY = ((int[])connectedDots[0])[1],
                maxY = ((int[])connectedDots[0])[1];

            for(int i = 1; i<connectedDots.Count; i++) {
                if(((int[])connectedDots[i])[0]<minX) {
                    minX=((int[])connectedDots[i])[0];
                } else if(((int[])connectedDots[i])[0]>maxX) {
                    maxX=((int[])connectedDots[i])[0];
                }

                if(((int[])connectedDots[i])[1]<minY) {
                    minY=((int[])connectedDots[i])[1];
                } else if(((int[])connectedDots[i])[1]>maxY) {
                    maxY=((int[])connectedDots[i])[1];
                }
            }

            return new int[] { minX, maxX, minY, maxY };
        }

        /////////////////////////////////////////////////////////////////////////////////////

        public ArrayList getConnectedPixelGroups() {
            return this.connectedPixelGroups;
        }

        public void setConnectedPixelGroups(ArrayList connectedPixelGroups) {
            this.connectedPixelGroups=connectedPixelGroups;
        }

        public ArrayList getConnectedPixelGroupPositions() {
            ArrayList connectedPixelGroupPositions = new ArrayList();

            for(int i = 0; i<this.connectedPixelGroups.Count; i++) {
                connectedPixelGroupPositions.Add(((ArrayList)this.connectedPixelGroups[i])[0]);
            }

            return connectedPixelGroupPositions;
        }

        public Matrix getFilteredMatrix() {
            
            Matrix filteredMatrix = new Matrix(this.matrix.Row, this.matrix.Col, Matrix.BLACK_WHITE);
            for(int i = 0; i<this.matrix.Row; i++) {
                for(int j = 0; j<this.matrix.Col; j++) {
                    filteredMatrix.pixels[i, j, 0]=Matrix.MAX_PIXEL;
                }
            }

            for(int i = 0; i<this.connectedPixelGroups.Count; i++) {
                drawPixelGroup(filteredMatrix, (ArrayList)(this.connectedPixelGroups[i]), Matrix.MIN_PIXEL);
            }

            return filteredMatrix;
        }

        private void drawPixelGroup(Matrix matrix, ArrayList connectedDots, int pixel) {
            foreach(int[] connectedDot in connectedDots) {
                matrix.pixels[connectedDot[0]-1, connectedDot[1]-1, 0]=pixel;
            }
        }

    }
}
