/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
* Date: 30-Sep-2016                                        *
* License: MIT License                                     *
***********************************************************/

using System;
using Egami.com.minhaskamal.matrix;

namespace Egami.com.minhaskamal.filtering {
	public class MedianFilterRunner {
		/**
		 * 
		 * @param matrix
		 * @param maskLength should be an odd number. best is 3.
		 * @return
		 */
		public static Matrix applyMedianFilter(Matrix matrix, int maskLength) {
			Matrix matrix2 = new Matrix(matrix.Row, matrix.Col, Matrix.BLACK_WHITE);

			int border = (maskLength-1)/2;
			matrix=MatrixEditor.createBorder(matrix, border, Matrix.MAX_PIXEL);

			int rows = matrix.Row,
				cols = matrix.Col;

			for(int i = border; i<rows-border; i++) {
				for(int j = border; j<cols-border; j++) {

					int[][] arrayRaw = MatrixUtilities.vectorize(
							matrix.subMatrix(i-border, i+maskLength-border, j-border, j+maskLength-border));
					int[] array = new int[arrayRaw.Length];
					for(int k = 0; k<array.Length; k++) {
						array[k]=arrayRaw[k][0];
					}

					matrix2.pixels[i-border, j-border, 0]=getMedian(array);
				}
			}

			return matrix2;
		}

		private static int getMedian(int[] array) {

			Array.Sort(array);
			int median = array[array.Length/2];

			return median;
		}
	}
}