/***********************************************************
* Developer: Minhas Kamal (minhaskamal024@gmail.com)       *
* Date: Sep-2016                                           *
* License: MIT License                                     *
***********************************************************/

using System;
using System.Collections;

namespace Egami.com.minhaskamal.utilityAlgorithms {
    public class ClusterMaker {

        /////////////////////////////////////////////////////////////////////////////////////////////////

        public static int[][] kMeansClustering(int[][] dataPoints, int k) {
            int[][] clusterMeans = new int[k][];
            Random random = new Random();
            for(int i = 0; i<k; i++) {
                clusterMeans[i]=(int[])dataPoints[random.Next(dataPoints.Length-1)].Clone();
            }

            int[][] newClusterMeans;
            do {
                int[] pointInClusters = assignPointToCluster(dataPoints, clusterMeans);
                newClusterMeans=calculateClusterMeans(dataPoints, pointInClusters, k);
            } while(change(clusterMeans, newClusterMeans));

            return clusterMeans;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////

        public static int[][] meanShiftClustering(int[][] dataPoints, int radious, int n) {
            int[][] clusterMeans = new int[n][];
            Random random = new Random();
            for(int i = 0; i<n; i++) {
                clusterMeans[i]=(int[])dataPoints[random.Next(dataPoints.Length-1)].Clone();
            }

            int[][] newClusterMeans;
            do {
                double[][] distaces = calculateAllDistances(dataPoints, clusterMeans);
                newClusterMeans=calculateClusterMeans(dataPoints, distaces, radious);
            } while(change(clusterMeans, newClusterMeans));

            return cleanDuplicacy(clusterMeans);
        }

        private static int[][] cleanDuplicacy(int[][] clusterMeans) {
            ArrayList cleanClusterMeans = new ArrayList();
            cleanClusterMeans.Add(clusterMeans[0]);

            bool arrayMatched;
            for(int i = 1; i<clusterMeans.Length; i++) {
                arrayMatched=false;

                foreach(int[] clusterMean in cleanClusterMeans) {
                    bool matched = true;
                    for(int p = 0; p<clusterMean.Length; p++) {
                        if(clusterMean[p]!=clusterMeans[i][p]) {
                            matched=false;
                            break;
                        }
                    }
                    if(matched) {
                        arrayMatched=true;
                        break;
                    }
                }

                if(!arrayMatched) {
                    cleanClusterMeans.Add(clusterMeans[i]);
                }
            }

            return (int[][])cleanClusterMeans.ToArray();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////

        public static int[] assignPointToCluster(int[][] dataPoints, int[][] clusterMeans) {
            double[][] distaces = calculateAllDistances(dataPoints, clusterMeans);
            int[] pointInCluster = findPointInCluster(distaces);

            return pointInCluster;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        private static double[][] calculateAllDistances(int[][] dataPoints, int[][] clusterMeans) {
            double[][] distaces = new double[dataPoints.Length][];

            for(int i = 0; i<dataPoints.Length; i++) {
				distaces[i] = new double[clusterMeans.Length];
                for(int j = 0; j<clusterMeans.Length; j++) {
                    distaces[i][j]=calculateDistance(dataPoints[i], clusterMeans[j]);
                }
            }

            return distaces;
        }

        private static double calculateDistance(int[] point1, int[] point2) {
            double distance = 0;

            for(int i = 0; i<point1.Length; i++) {
                distance+=Math.Pow(point1[i]-point2[i], 2);
            }
            distance=Math.Sqrt(distance);

            return distance;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////

        private static int[] findPointInCluster(double[][] distaces) {
            int[] pointInCluster = new int[distaces.Length];

            for(int i = 0; i<pointInCluster.Length; i++) {
                pointInCluster[i]=findIndexOfMinValue(distaces[i]);
            }

            return pointInCluster;
        }

        private static int findIndexOfMinValue(double[] values) {
            int minIndex = 0;
            double minValue = values[minIndex];

            for(int i = 1; i<values.Length; i++) {
                if(minValue>values[i]) {
                    minIndex=i;
                    minValue=values[minIndex];
                }
            }

            return minIndex;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////

        private static int[][] calculateClusterMeans(int[][] dataPoints, int[] pointInClusters, int k) {
            int[][] clusterMeans = new int[k][];
            double[][] clusterSum = new double[clusterMeans.Length][];
			for(int i=0; i<k; i++) {
				clusterMeans[i] = new int[dataPoints[0].Length];
				clusterSum[i] = new double[dataPoints[0].Length];
            }

            int[] clusterMembers = new int[clusterMeans.Length];
            for(int i = 0; i<dataPoints.Length; i++) {
                for(int j = 0; j<dataPoints[0].Length; j++) {
                    clusterSum[pointInClusters[i]][j]+=dataPoints[i][j];
                }
                clusterMembers[pointInClusters[i]]++;
            }

            for(int i = 0; i<clusterMeans.Length; i++) {
                for(int j = 0; j<clusterMeans[0].Length; j++) {
                    clusterMeans[i][j]=(int)(clusterSum[i][j]/clusterMembers[i]);
                }
            }

            return clusterMeans;
        }

        private static int[][] calculateClusterMeans(int[][] dataPoints, double[][] distaces, int radious) {
            int[][] clusterMeans = new int[distaces[0].Length][];

            double[][] clusterSum = new double[clusterMeans.Length][];
			for(int i = 0; i<distaces[0].Length; i++) {
				clusterMeans[i]=new int[dataPoints[0].Length];
				clusterSum[i]=new double[dataPoints[0].Length];
			}

			int[] clusterMembers = new int[clusterMeans.Length];
            for(int i = 0; i<clusterMeans.Length; i++) {
                for(int j = 0; j<distaces.Length; j++) {
                    if(distaces[j][i]<=radious) {
                        for(int k = 0; k<dataPoints[0].Length; k++) {
                            clusterSum[i][k]+=dataPoints[j][k];
                        }
                        clusterMembers[i]++;
                    }
                }
            }

            for(int i = 0; i<clusterMeans.Length; i++) {
                for(int j = 0; j<clusterMeans[0].Length; j++) {
                    clusterMeans[i][j]=(int)(clusterSum[i][j]/clusterMembers[i]);
                }
            }

            return clusterMeans;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////

        private static bool change(int[][] clusterMeans, int[][] newClusterMeans) {
            bool isChanged = false;

            for(int i = 0; i<clusterMeans.Length; i++) {
                for(int j = 0; j<clusterMeans[0].Length; j++) {
                    if(clusterMeans[i][j]!=newClusterMeans[i][j]) {
                        clusterMeans[i][j]=newClusterMeans[i][j];
                        isChanged=true;
                    }
                }
            }

            return isChanged;
        }

    }
}